# Cash Denominate
The goal of this program is to return an list of denomination value based on given value by user.
the input value will be filtered by a given format and will give a warning if the input doesnt meet the required format.

### input 
```
12500
```

### output
```
1 pcs 10000
2 pcs 1000
1 pcs 500
```   

### valid input
```
Examples of valid inputs with their canonical equivalents 18.215 (18215), Rp17500 (17500),
Rp17.500,00 (17500), Rp 120.325 (120325), 005.000 (5000), 001000 (1000)
```

### invalid format
```
Examples of invalid inputs: 17,500 (invalid separator), 2 500(invalid separator), 3000 Rp
(valid character in wrong position), Rp (missing value)
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```